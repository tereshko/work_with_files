#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 8

int openFile();
int createFile();
void readFile( int openedFile, double buffer);
double getValue();
size_t writeFile( int openedFile, double value);
void closeFile( int openedFile);

int main (int argc, char** argv) {

	int openedFile = openFile();
	
	if( 0 < openedFile) {
	
		double buffer;
	
		readFile( openedFile, buffer);

		lseek( openedFile, 0, SEEK_SET);
		
		double value = getValue();
		
		size_t byteWrite = writeFile( openedFile, value);
		
		if( 0 < byteWrite) {
				
			closeFile( openedFile);
			
		}
	
	} else {

		std::cout << "Cannot open file\n";

	}

	return 0;
}


int openFile() {
	
	int fileId;
	
	fileId = open( "bin.txt", O_RDWR);
	
	if( -1 == fileId) {
	
		fileId = createFile();

	}	

	return fileId;
}

int createFile() {

	int fileId;
	std::cout << "\nFile not found. We create him\n";
 	fileId = open( "bin.txt", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR) ;
 	
 	return fileId;
}

void readFile( int openedFile, double buffer) {

	size_t byteRead = read( openedFile, &buffer, BUFFER_SIZE);
		
	if( 0 == byteRead){
		
		std::cout << "\nSorry, file is empty\n";
		
	} else {
		
		std::cout.precision(17);
		std::cout <<"\n\nFirst char in the file: " << buffer << "\n\n";
		
	}
}

double getValue() {

	double value;
	std::cout << "\nPlease enter value for save in file: ";
	std::cin >> value;
	
	return value;

}

size_t writeFile( int openedFile, double value) {

	size_t byteWrite = write( openedFile, &value, BUFFER_SIZE);
	
	if( 0 > byteWrite) {
	
		std::cout << "Cannot write to file\n";
			
		closeFile( openedFile);
	
	}
	
	return byteWrite;
}

void closeFile( int openedFile) {
	
	int closeFile = close( openedFile);
			
	if( 0 > closeFile) {
			
		std::cout << "\nCannot close file\n";
		
		return;
			
	}

}

